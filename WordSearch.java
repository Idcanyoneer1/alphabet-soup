import java.io.File;                    // Import the File class
import java.io.FileNotFoundException;   // Import this class to handle errors
import java.util.Scanner;               //Import for loading in file
import java.util.Vector;                //Import for my vectors

/*
 * //ToDo Nothing it is all done :)
 */

class WordSearch {

    public static void main(String[] args) {
        //-------------Varibles-------------------------------------------
        int fileX;
        int fileY;
        int wordCount;
        Vector<Vector<Character>> wordSearch = new Vector<Vector<Character>>();
        Vector<String> words = new Vector<String>();
        //----------------------------------------------------------------
        


        try {

            Scanner myObj2 = new Scanner(System.in);  // Create a Scanner object
            System.out.println("Enter file name: ");

            String fileName = myObj2.nextLine();  // Read user input
            myObj2.close();

            //make object
            File myObj = new File(fileName);//THIS IS THE INPUT FOR THE PROGRAM
            Scanner myReader = new Scanner(myObj);

            //read in the first line of the inputted file and load it into a splitter which loads our array size.
            String firstLine = myReader.nextLine();
            String[] num = firstLine.split("x");
            fileY = Integer.valueOf(num[0]);
            fileX = Integer.valueOf(num[1]);
            //--------------------------------------------------------------------------------------------------
            
            //-displays the file----------------------------------------------------------------------------------------
            for (int j = 0; j < fileY; j++) {
                //Read in the row to data and make it all Uppercase.
                String data = myReader.nextLine().toUpperCase();

                //add a vector of charactors to the current row [j]
                wordSearch.add(j, new Vector<Character>());

                //And finaly split the row into the individual entries and store them in the array of strings splitRow[]
                String splitRow[] = data.split(" ");

                //load the row with the array of strings casted to charactors
                for(int i = 0; i < fileX; i++){
                    wordSearch.get(j).add(i, splitRow[i].charAt(0));
                }
            }
            //----------------------------------------------------------------------------------------------------------

            //Get Words at the end of the file and display them in the list---------------
            wordCount = 0;
            while(myReader.hasNextLine()){
                words.add(wordCount, myReader.nextLine());
                wordCount++;
            }
            //----------------------------------------------------------------------------

            //Clean up--------------------
            myReader.close();
            //----------------------------

        // If the file name/path isn't recognized then it goes here and says "an error occurred."
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred in loading the file, please try a new file name/path");
            e.printStackTrace();
        }
        /*
         * The file is now loaded into the appropriate vectors and integers.
         * Those are the wordSearch vector of characters
         * the x and y integers of the vector wordSearch
         * the vector of words to find named words
         * and finally the number of words we are going to find.
         * 
         * New variables created for this section are the Strings wordStartPos
         * and the in arrays to hold temporary outputs so I don't run the search functions over and over again.
         */
        //Variables--------------------------------
        int wordStartPosX;
        int wordStartPosY;
        int temp2LongArrayStart[]; // this only exists so that I don't run a function over and over again.
        int temp2LongArrayEnd[] = {-10, -10};   // same ---^
        //-----------------------------------------

        //This statment can be read as "for every word in the vector words" so it will print out the location
        //of the word first word then the second and so on.
        for (String word : words) {

            //Find the first instance of the first letter starting at 0, 0 searching left to right top to bottom
            //and loading the coordinates into the wordStartPos
            temp2LongArrayStart = FindNextCharacter(word.charAt(0), -1, 0, wordSearch);
            wordStartPosX = (temp2LongArrayStart[0]);
            wordStartPosY = (temp2LongArrayStart[1]);

            //quick check if that word can even be in the list if it isn't then just move on.
            if(wordStartPosX == -10){
                System.out.println(word + " Not Found");
                continue;
            }

            //While we have a Valid position (wordStartPosX != -10) check around it and return the first correct
            //solution, if none are found then set exit.
            while(wordStartPosX != -10){
                //Check if the current letter is right (returns end location if it is right)
                temp2LongArrayEnd = checkEightDirections(word, wordStartPosX, wordStartPosY, wordSearch);

                //If the checkEightDirections function resulted in a fail (case -10) then find a new starting letter...
                if(temp2LongArrayEnd[0] == -10 || temp2LongArrayEnd[1] == -10){
                    //This moves to the next letter in the list
                    temp2LongArrayStart = FindNextCharacter(word.charAt(0), wordStartPosX, wordStartPosY, wordSearch);
                    wordStartPosX = (temp2LongArrayStart[0]);
                    wordStartPosY = (temp2LongArrayStart[1]);
                }
                //break once the word has been found!
                if(temp2LongArrayEnd[0] != -10){
                    break;
                }
            }

            //Another quick check if that word can even be in the list if it isn't then just move on.
            if(wordStartPosX == -10){
                System.out.println(word + " Not Found");
                continue;
            }

            //Output, this outputs in the way that the example does, I named by positions x and y instead of colum and row (x = colum)(y = row)
            System.out.println(word + " " + wordStartPosY + ":" + wordStartPosX + " " + temp2LongArrayEnd[1] + ":" + temp2LongArrayEnd[0] );
        }
    }


    //-------------MY-SPECIAL-FUNCTIONS-------------------



    /*
     * This returns the end position of the word provided IF AND ONLY IF the correct starting letter is provided in x, and y.
     * it gets the word from GivenWord and is searching the two dimensional vector characterVector.
     * 
     * if it does not find the character then it will output [-10, -10]
     * 
     * Yes this function is really long and repetitive, but it is actually really easy to debug, if you know which word is not being found.
     * I would imagine that it is possible to solve this same problem with recursion but recursion is hard to read so I elected to individually
     * check each of the 8 directions 'manually' the down side would be if I added a third dimension to the word search, and that doesn't really
     * apply in this situation. but could be fun to add later for the fun of it.
     */
    static int[] checkEightDirections(String GivenWord, int x, int y, Vector<Vector<Character>> characterVector){
        //return variable.---------------
        int toReturn[] = {-10, -10};
        //-------------------------------
        
        toReturn = checkDirection(GivenWord, x, y, characterVector, 1, 0);   //right
        if(toReturn[0] != -10){
            return toReturn;
        }
        toReturn = checkDirection(GivenWord, x, y, characterVector, 0, 1);   //down
        if(toReturn[0] != -10){
            return toReturn;
        }
        toReturn = checkDirection(GivenWord, x, y, characterVector, -1, 0);  //left
        if(toReturn[0] != -10){
            return toReturn;
        }
        toReturn = checkDirection(GivenWord, x, y, characterVector, 0, -1);  //up
        if(toReturn[0] != -10){
            return toReturn;
        }
        toReturn = checkDirection(GivenWord, x, y, characterVector, 1, 1);   //down/right
        if(toReturn[0] != -10){
            return toReturn;
        }
        toReturn = checkDirection(GivenWord, x, y, characterVector, 1, -1);  //up/right
        if(toReturn[0] != -10){
            return toReturn;
        }
        toReturn = checkDirection(GivenWord, x, y, characterVector, -1, 1);  //down/left
        if(toReturn[0] != -10){
            return toReturn;
        }
        toReturn = checkDirection(GivenWord, x, y, characterVector, -1, -1); //up/left
        if(toReturn[0] != -10){
            return toReturn;
        }
        //if we get here we are returning -10, -10 and saying the word was not found at this instance of the character.
        return toReturn;
        
    }
    static int[] checkDirection(String GivenWord, int startX, int startY, Vector<Vector<Character>> characterVector, int xMovement, int yMovement) {
        int x = startX;
        int y = startY;
        char GivenWordList[] = GivenWord.toCharArray();
        int intToReturn[] = {-10, -10};

        for ( char c : GivenWordList) {
                //if the current x and y of the characterVector equals the letter we want it to be then step in
                if(characterVector.get(y).get(x) == c){
                    //if we are looking at the final character then return the position of x and y
                    if(GivenWord.charAt(GivenWord.length()-1) == characterVector.get(y).get(x) && (Math.abs(x-startX) == GivenWordList.length-1 || Math.abs(startY - y)== GivenWordList.length-1)){
                        //set and return x and y
                        intToReturn[0] = x;
                        intToReturn[1] = y;
                        return intToReturn;
                    }
                    //if the next element lies outside the grid, then one of these will be true and it will break.-----
                    if(y-1 < 0 && yMovement == -1){
                        //break, up doesn't work
                        break;
                    }
                    if(x+1 >= characterVector.get(y).size() && xMovement == 1){
                        //break, right doesn't work
                        break;
                    }
                    if(y+1 >= characterVector.size() && yMovement == 1){
                        //break, down doesn't work
                        break;
                    }
                    if(x-1 < 0 && xMovement == -1){
                        //break, left doesn't work
                        break;
                    }
                    //--------------------------------------------------------------------------------------------------
                    //if the current character was right, next one is in the grid, and we are not looking at the next character then move to the next char
                    y=y+yMovement;
                    x=x+xMovement;
                    continue;
                }
                //if the current character is wrong then break
                break;
            }
        return intToReturn;
    }


    /*
     * This Function will take a character (c), a position (x, y), and a vector of vectors (characterVector)
     * and return a integer array of length 2 that position is the position of the next character after the imputed position
     * So this can be used to check for the first  postion by giving the function the position x = -1 and y = 0. this
     * is because it checks the NEXT position after the one imputed.
     * 
     * If there is no more items to find it outputs [-10, -10] 
     */
    static int[] FindNextCharacter(char c, int x, int y, Vector<Vector<Character>> characterVector){
        //change to the next charactor in the vector---
        if(x + 1 < characterVector.get(y).size()){
            ++x;
        }else{
            x = 0;
            ++y;
        }
        //---------------------------------------------

        for(int i = y; i < characterVector.size(); i++){
            if(-1 == characterVector.get(i).indexOf(c, x)){
                x = 0;
                continue;
            }else{
                y = i;
                x = characterVector.get(y).indexOf(c, x);
                int toReturn[] = {x, y};
                return toReturn;
            }
        }
        int toReturn[] = {-10, -10};
        return toReturn;
    }
}