I used Java...

I used VS Code for my IDE with no external build tools.

I have included two input files the first is the example from the challenge, and the second is one I used to try and break my code
if run both GOOGLE, and HOWDY will not be found because they are not actually in word search.

The file you want to run should be in the folder with the .java file. You could enter a full file path but that is a pain.
Input file name should be entered as "input.txt" not "input"
for example I have included two sample input files to run the input.txt file you would run the program, it would then say this:

```
Enter file name:
input.txt
```
and the output would be:
```
HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1
```

-----------------------------------------------------------------------
My launch.json contains the following
```
{
    "version": "0.2.0",
    "configurations": [
        
        {
            "type": "java",
            "name": "Current File",
            "request": "launch",
            "mainClass": "${file}"
        },
        {
            "type": "java",
            "name": "WordSearch",
            "request": "launch",
            "mainClass": "WordSearch",
            "projectName": "alphabet-soup_2b148311"
        }
    ]
}
```
-----------------------------------------------------------------------